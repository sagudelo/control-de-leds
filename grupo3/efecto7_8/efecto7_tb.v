
`timescale 1 ns / 1 ps

module efecto7_tb;
	reg clk = 0, rst, efecto7en;
	reg[3:0] addr = 0, idx;
	reg[15:0] datoram, i;
	wire[3:0] addram;   
	wire[15:0] data; 
	
	efecto7 uut ( .clk(clk), .rst(rst), .addr(addr), .efecto7en(efecto7en), .data(data), .addram(addram), .datoram(datoram));

	always
		clk = #10 ~clk; // señal de reloj

    initial begin
    	#0 rst = 1; efecto7en = 0;
    	#20 rst = 0; efecto7en = 1;
    	for(i = 2; i < 12; i = i+1) 
    		#20 datoram = i;
    	#20 efecto7en = 0;
    	#2000 efecto7en = 1;
    	for(i = 2; i < 12; i = i+1) 
    		#20 datoram = i;
    	#20 efecto7en = 0;
    end
    
    always begin
    	#20 addr = addr + 1;
    	if(addr == 10) 
    		addr = 0;	
    end
    
	initial begin: TEST_CASE
		$dumpfile("efecto7.vcd");
		$dumpvars(0, efecto7_tb);
		for (idx = 0; idx < 4; idx = idx + 1) $dumpvars(0, uut.temp[idx]);

		#6400 $finish;
	end
endmodule
