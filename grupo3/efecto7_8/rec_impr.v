/*
`timescale 1 ns / 1 ps

module rec_impr( clk, dataram, init_crt, fin_crt, dir_inc, addr, clk_col, init_col, fin_col, leds, rom);

input clk, init_crt;
input[3:0] addr;//ram
input[15:0] dataram;
reg [3:0] crt;
output	reg fin_crt;	
output reg [11:0] dir_inc;
parameter maxcrt=10; // maximo caracter

input clk_col, rom, init_col;
output reg fin_col;
output reg [15:0] leds;
wire [15:0] rom_temp;//??
reg [3:0] col;
parameter maxcol=16;

always @(posedge clk) begin// recorrido caracter
    if (init_crt) begin 
    	crt=4'b0000;
        fin_crt<=1'b0;
  end 
    else 
      if (crt<maxcrt)
         crt<=crt+1;
    else crt<=crt;
         fin_crt<=1'b1;
  end

always @(crt) begin
     dir_inc<= dataram[crt];//buscar en la ram de las direcciones la que tiene sergio 
  end



always @(posedge clk_col) begin // recorrido columna
    if (init_col == 1)begin 
        col=4'b0000;
       fin_col<=1'b0;
   end 
    else 
      if (col<maxcol)col<=col+1;
    else col<=col;
         fin_col<=1'b1;
   end

always @(col) begin
     rom_temp <= dir_inc+col;
     assign leds = rom_temp;
   end
endmodule   
*/


`timescale 1 ns / 1 ns

module rec_impr(clk, clk_out, rst, addram, datoram, addrom, datorom, ledsout, efecto7en, efecto8en);
	input clk, clk_out, rst, efecto7en, efecto8en;
	input[15:0] datoram;
	input[15:0] datorom;
	output [3:0] addram;
	output reg [11:0] addrom;
	output[15:0] ledsout;
	
	assign ledsout = datorom; // Impresion de ROM a salida LEDs
	
	wire[15:0] data7, data8;
	reg[3:0] addr, ctr16;
	
	efecto7 uut7 ( .clk(clk), .rst(rst), .addr(addr), .efecto7en(efecto7en), .data(data7), .addram(addram), .datoram(datoram));
	efecto8 uut8 ( .clk(clk), .rst(rst), .addr(addr), .efecto8en(efecto8en), .data(data8), .addram(addram), .datoram(datoram));
	
	always@(efecto7en or efecto8en) begin // Multiplexor para ROM
		if(efecto7en)
			addrom <= #1 data7[11:0] + ctr16;
		else if(efecto8en)
			addrom <= #1 data8[11:0] + ctr16;
	end
	
	always@(posedge clk_out) begin // Contador de direccion memoria temporal
		if(rst == 1)
			addr <= #1 0;
		else if(ctr16 == 15)
			addr <= #1 addr + 1;
	end
	
	always@(posedge clk_out) begin // Contador de direccion memoria ROM
		if(rst == 1)
			ctr16 <= #1 0;
		else
			ctr16 <= #1 ctr16 + 1;
	end

endmodule
