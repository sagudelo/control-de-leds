
`timescale 1 ns / 1 ns

module efecto7(clk, rst, addr, efecto7en, data, addram, datoram);
	input clk, rst, efecto7en;
	input[3:0] addr;
	input[15:0] datoram;
	output reg [3:0] addram;
	output[15:0] data;
	
	reg[15:0] temp [3:0];
	reg cdir, rstdir, encol, rstcol, entemp, enram;
	reg[3:0] dir; 
	reg[15:0] col;
	
	wire [3:0] addramtemp;
	assign addramtemp = addram;
	
	assign data = temp[addr];
	
	always @(posedge clk) begin // memoria temporal 16x10bits, que guarda direccion inicial de cada letra;
		if(entemp)
			temp[(dir == 15) ? 0 : dir+1] <= #1 col;
		else if(enram)
			temp[addramtemp] <= #1 datoram;
	end
	
	always @(posedge clk) begin // registro de 16 bits temporal
		if(rst | rstcol)
			col <= #1 0;
		else if(encol)
			col <= #1 temp[dir];
	end
	        
	always @(posedge clk) begin // direccion de memoria temporal
		if(rstdir | rst)
			dir <= #1 8;
		else if(cdir)
			dir <= #1 dir - 1;
	end
	
	always @(posedge clk) begin // direccion de memoria ram
		if(rstdir | rst)
			addram <= #1 0;
		else if(enram)
			addram <= #1 addram + 1;
	end
	
	// Maquina de estados
	reg[2:0] estado;
	always @(estado) begin // Logica de salida
		case(estado)
			0: begin
				cdir = 0; rstdir = 1; encol = 0; entemp = 0; rstcol = 0; enram = 0; // resetear dir
			end
			1: begin
				cdir = 0; rstdir = 0; encol = 0; entemp = 0; rstcol = 0; enram = 1; // cargar temp desde RAM
			end
			2: begin
				cdir = 0; rstdir = 0; encol = 1; entemp = 0; rstcol = 0; enram = 0;// cargar col
			end
			3: begin
				cdir = 0; rstdir = 0; encol = 0; entemp = 1; rstcol = 0; enram = 0; // cargar temp con efecto
			end
			4: begin
				cdir = 1; rstdir = 0; encol = 0; entemp = 0; rstcol = 0; enram = 0;// modificar direccion
			end   
			5: begin
				cdir = 0; rstdir = 0; encol = 0; entemp = 0; rstcol = 1; enram = 0;// resetear columna
			end
			6: begin
				cdir = 0; rstdir = 0; encol = 0; entemp = 1; rstcol = 1; enram = 0;// cargar temp 0
			end
		endcase
	end
	
	always @(posedge clk) begin // Logica de estado siguiente
		if(rst)
			estado = 0;
		else begin
			case(estado)
				0: estado <= #1 (efecto7en == 1) ? 1 : 0;
				1: estado <= #1 (addram == 9) ? 2 : 1;
				2: estado <= #1 3;
				3: estado <= #1 4;
				4: estado <= #1 (dir == 0)? 5 : 2;
				5: estado <= #1 6;
				6: estado <= #1 0;
			endcase	
		end
	end
	
endmodule
	
