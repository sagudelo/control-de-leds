
`timescale 1 ns / 1 ps

module efecto8_tb;
	reg clk = 0, rst, efecto8en;
	reg[9:0] addr = 0, idx;
	reg[15:0] dataram, i;
	wire[9:0] addram;   
	wire[15:0] data; 

	efecto8 uut ( .clk(clk), .rst(rst), .addr(addr), .efecto8en(efecto8en), .data(data), .addram(addram), .datoram(datoram));
	
	always
		clk = #10 ~clk; // señal de reloj

    initial begin
    	#0 rst = 1; efecto8en = 0;
    	#20 rst = 0; efecto8en = 1;
    	for(i = 1; i < 11; i = i+1) 
    		#20 datoram = i;
    	#20 efecto8en = 0;
    	#2000 efecto8en = 1;
    	for(i = 1; i < 11; i = i+1) 
    		#20 datoram = i;
    	#20 efecto8en = 0;
    end
    
    always begin
    	#20 addr = addr + 1;
    	if(addr == 10) 
    		addr = 0;	
    end
    
	initial begin: TEST_CASE
		$dumpfile("efecto8.vcd");
		$dumpvars(0, efecto8_tb);
		for (idx = 0; idx < 4; idx = idx + 1) $dumpvars(0, uut.temp[idx]);

		#6400 $finish;
	end
endmodule
