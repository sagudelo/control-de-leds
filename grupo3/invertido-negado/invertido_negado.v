`timescale 1ns / 1ps
module impresion( clk,load_leds,leds,ledsOut);
	input 		clk;
	input			load_leds;
	input [15:0]	 leds;
	output reg [15:0] ledsOut;
	reg [15:0] 	temp;
	reg [3:0] 	con_tem;
	 

always @(posedge clk)
begin
		if (load_leds == 1)begin
		temp<=leds;
		con_tem<=0;
		end else temp <= temp;

		
		 ledsout[15]<=~tem[0];
		 ledsout[14]<=~tem[1];
		 ledsout[13]<=~tem[2];
		 ledsout[12]<=~tem[3];
		 ledsout[11]<=~tem[4];
		 ledsout[10]<=~tem[5];
		 ledsout[9]<=~tem[6];
		 ledsout[8]<=~tem[7];
		 ledsout[7]<=~tem[8];
		 ledsout[6]<=~tem[9];
		 ledsout[5]<=~tem[10];
		 ledsout[4]<=~tem[11];
		 ledsout[3]<=~tem[12];
		 ledsout[2]<=~tem[13];
		 ledsout[1]<=~tem[14];
		 ledsout[0]<=~tem[15];
end


